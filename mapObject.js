let mapObject = (obj, cb) => {

    let result = obj;

    if (!typeof result == "object" || !result || !cb) {
        return [];
    }
    else {

        for (let data in result) {
            result[data] = cb(result[data]);
        }

        return result;
    }
}
module.exports = mapObject;