let pairs = (obj) => {

    if (!typeof (obj) === "object" || !obj) {
        return [];
    }
    else {
        let result = [];

        for (let data in obj) {
            result.push([data, obj[data]]);
        }

        return result;
    }
}
module.exports = pairs;