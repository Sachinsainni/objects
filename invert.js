let invert = (obj) => {

    if (!obj || !typeof obj == "object") {
        return [];
    }
    else {
        let result = {};

        for (let data in obj) {
            result[obj[data]] = data;
        }

        return result;
    }
}
module.exports = invert;