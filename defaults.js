let defaults = (obj, defaultProps) => {

    let result = obj;

    if (!typeof result == "object" || !result || !defaultProps) {
        return [];
    }
    else {
        for (let data in defaultProps) {

            if (data in obj) {
                continue;
            }
            else {
                result[data] = defaultProps[data];
            }
        }
        return result;
    }
}
module.exports = defaults;