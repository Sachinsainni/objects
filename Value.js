let value = (obj) => {

    if (!typeof (obj) == "object" || !obj) {
        return [];

    }
    else {
        let result = [];

        for (let data in obj) {
            result.push(obj[data]);
        }

        return result;
    }
}
module.exports = value;